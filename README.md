# alta3research-ansible-cert
Alta3 Research Ansible Certification - Grading Request

## notes
This play book contains some syslog operations may be useful in my environment such as pushing a config file from controller, fetching logs from hosts, and removing old log files from the host.
This playbook used planetexpress hosts from lab environment to simulate some syslog servers


## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* **Lim Zhyue Ee** - *Initial work*


